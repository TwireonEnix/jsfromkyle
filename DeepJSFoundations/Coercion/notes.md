# Coercion

Happens all over the place even though this is a really misunderstood part of javascript that other people say
it causes erratic behavior but is because they do not understand what is happening under the hood.

This happens often due to a condition called 'Operation overloading' where depending which types are involved
in the operation. different things happen.

For example: In the specification of the + (plus) operator it states that if any string is detected in the
operation it performs a string concatenation. On the other hand, if both arguments are numbers it performs a
numeric addition.

This overloading has string concatenation preference which means that it will try that first.

As a side note, the plus operator has other function as a unary operator that will convert into a number
anything it precedes. +'123' === 123 Provided that the string is a well formed numeric operation

On the contrary, the - (minus) operator is only declared to be used for numbers.

However the best (I think) way to explicitly convert a value into string, without recurring to coercion is
using the fundamental object String as a function

const num = 10

String(num);

# Boxing

Is a form of implicit coercion, it's the way we can access properties of primitive values, like

> 'Hello'.length // 5

When trying to access these properties, javascript coerces the primitive value into their object counterpart.

All programming languages have type conversions, because it is absolutely necessary.

![](2020-09-16-16-40-33.png)

![](2020-09-16-16-42-42.png)

We do not deal with these types conversion corner cases by avoiding them, instead we should adopt a style that
makes value types plain and obvious.

## Implicit coercions

Implicit != Magic

Implicitness means that is using abstraction, hiding the unnecessary details. This increases clarity.

Useful: When the reader is focused on what is important.

Dangerous: when the reader cannot tell what is going to happen.

Better: when the reader understands the code
