# Deep Javascript Foundations

## There are three pillars of Javascipt

### 1. Types

- Primitive types
- Abstract operations
- Coercion
- Equality
- Typescript, Flow, etc.

### 2. Scope

- Nested Scope
- Hoisting
- Closure
- Modules

### Objects (Oriented)

- this
- class{}
- Prototypes
- OO vs OLOO
