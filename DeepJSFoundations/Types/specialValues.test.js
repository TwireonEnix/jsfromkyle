const { assert } = require('chai');
const { isFalse, isTrue, strictEqual } = assert;

const isEq = (a, b) => strictEqual(a, b);

describe('Special values for primitive types', () => {
  it('Nan Analysis', () => {
    const age = Number('0o35'); // Octal
    isEq(age, 29);

    const nextAge = Number('30');
    isEq(nextAge, 30);

    const otherAge = Number('n/a'); // NaN

    assert.isFalse(otherAge === otherAge); // FunFact, the NaN value is the only value in the existence of Javascript
    // that has no identity.

    isEq(isNaN(age), false);
    isEq(isNaN(otherAge), true);

    isEq(isNaN('n/a'), true); // how?
    isEq(Number.isNaN('n/a'), false); // why?
    // Because the spec states that isNaN as a function will coerce the value to a number, and then checks if the result is the NaN VALUE
    // The utility of the Number interface does not make this coercion, therefore the string is not a NaN value, it is just a string

    isEq(typeof NaN, 'number'); // why? because the expectation of any numeric operation is a number, therefore, the value nan is
    // actually a way to say 'invalid number', not what the name suggest as not a number. weird huh?
  });

  it('Negative Zero', () => {
    // If asked to a mathematician, he or she will say that it does not exist. But in programming a negative zero has
    // sense. It is the value zero with the sign bit on.
    const negZ = -0;
    isTrue(negZ === -0);

    isEq(negZ.toString(), '0'); // what? One of the historical weirdness of javascript because it was supposed to help the developers
    // avoid inconsistencies with something that would not make sense in the real world.

    isTrue(negZ === 0); // triple equal lies
    isFalse(negZ > 0);
    isFalse(negZ < 0);

    // Quadruple equals?
    isTrue(Object.is(negZ, -0));
    isFalse(Object.is(negZ, 0));
  });
});
