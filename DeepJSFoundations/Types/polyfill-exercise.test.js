const { assert } = require('chai');
const { isTrue, isFalse } = assert;

/** Here we will define a polyfill for Object.is
 * 1. It should take two parameters
 * 2. It should return true if the passed in parameters are the same value, not just ===, or false otherwise.
 * 3. For NaN testing we can use Number.isNaN but lets find a solution without any utility
 * 4. For '-0' testing no built in utility exists, but we can use -Infinity
 * 5. If parameters are other values just test them for strict equality
 * 6. Object.is is forbidden.
 */

const objectIs = (paramOne, paramTwo) => {
  const isNegZero = v => v === 0 && 1 / v === -Infinity;
  const isNan = v => v !== v; // in js a NaN is the only value in existence that is not equal to itself.

  const check = fn => [paramOne, paramTwo].map(fn);

  const [oneNegZero, twoNegZero] = check(isNegZero);
  if (oneNegZero || twoNegZero) return oneNegZero && twoNegZero;

  const [oneNan, twoNan] = check(isNan);
  return oneNan && twoNan ? true : paramOne === paramTwo;
};

describe('Exercise: Writing an Object.is', () => {
  it('Test Cases', () => {
    isTrue(objectIs(42, 42));
    isTrue(objectIs('foo', 'foo'));
    isTrue(objectIs(false, false));
    isTrue(objectIs(null, null));
    isTrue(objectIs(undefined, undefined));
    isTrue(objectIs(NaN, NaN));
    isTrue(objectIs(-0, -0));
    isTrue(objectIs(0, 0));

    isFalse(objectIs(-0, 0));
    isFalse(objectIs(0, -0));
    isFalse(objectIs(NaN, 0));
    isFalse(objectIs(0, NaN));
    isFalse(objectIs('42', 42));
  });
});
