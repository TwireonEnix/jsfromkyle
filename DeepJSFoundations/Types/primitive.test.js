const { assert } = require('chai');

describe('Verify the types of values that a variable has', () => {
  const expectTo = (v, t) => assert.strictEqual(typeof v, t);

  it('will check primitive types', () => {
    /** The typeof operator always return a string from an enum array, and never an empty string  */

    /** Here we do not ask, what is the type of x, but what is the type of the value of x */
    let x;
    expectTo(x, 'undefined'); // undefined is like a default value.
    x = '1';
    expectTo(x, 'string');
    x = 1;
    expectTo(x, 'number');
    x = true;
    expectTo(x, 'boolean');
    x = {};
    expectTo(x, 'object');
    x = Symbol();
    expectTo(x, 'symbol');
  });

  it('will show some caveats', () => {
    let x = null;
    expectTo(x, 'object'); // Why? because of a specification rule, but it was must certainly a bug. The rule
    // stated that if we need to remove the primitive value of a variable we should use undefined, but if we need to remove
    // an object reference we should use null, that's why it returned an object.

    /** Here it is useful to get function when a function is analyzed, but with arrays not so much. These are caveats of the
     * Javascript history. With arrays we can use Array.isArray().
     *
     */
    x = () => {};
    expectTo(x, 'function');

    x = [1, 2, 3];
    expectTo(x, 'object');

    /** Recently (when?) added to the specification! A bigint is a value that can grow infinitely large. However they do not play
     * nicely with the primitive and regular old Number.
     */
    x = 12n;
    expectTo(x, 'bigint');

    const z = typeof y;
    expectTo(z, 'string');
    assert.strictEqual(z, 'undefined');
  });
});
