## In Javascript, everything is an object

This statement is false. Because of how everything could be and behave as a object, but not everything is an
object.

The basic ECMAScript Language primitive types are:

- Undefined
- Null
- Boolean
- String
- Symbol
- Number
- Object

### What's a primitive type?

The undefined type has only one value, which is undefined. Heh

The string type is a primitive also, not to be confused with an String object instantiated like in Java. Just
a string literal.

Number refers to all JS numbers

Boolean, JS only has two values for boolean, true and false.

Object, is also a primitive type, but it has subtypes, where almost all misconceptions live.

So a type is in essence a set of intrinsic characteristics that we expect to be able to do with that value.
The type is not assigned to a variable, but to a _value_.

There is also another primitive type called symbol. That is not commonly used in a day to day basis, but is a
way to create pseudo private keys in objects. Not really private though.

There are other things that may behave like types:

- undeclared?
- null?
- function? (have a very specific behavior, subtype of the object type. A callable object).
- arrays? (subtype of the object type)
- bigint?

> All of the above have a certain characteristic, with the exception of object, function and array, the other
> types are NOT objects.

// see tests

## Undefined vs Undeclared

When we try to access a declared variable that has no value like: let x;

The typeof operator will always return the string 'undefined'. There should be a string value called
'undeclared' to address the typeof in variables that haven't even been declared in any of the available scopes
in the workspace. As a side note typeof is the only operator that will not throw an error if we pass an
undeclared variable.

There is another state of emptiness called uninitialized (also known as TDZ, temporal dead zone).

## Special values

- NaN: Comes as Not a Number, essentially is better thought as an invalid number

- -0: Signed or Negative Zero.

# Fundamental Objects

> aka Built-in Objects or native Functions

These are the built in fundamental Objects, to construct any we must use the keyword 'new'.

- Object()
- Array()
- Function()
- Date()
- RegExp()
- Error()

Other are

- String()
- Number()
- Boolean()

However, we should use these last 3 as functions and not as constructors. But basically I think the ONLY
situation where we need to use the 'new' keyword is when creating a date. Only here. In other places is
unnecessary.

## Abstract operations

In the ECMAScript specification the type conversion is done automatically through abstract operations. This
feature is also, and more commonly known as 'coercion'.

### toString()

It takes any value and returns the representation of that value in string form. Almost every value has a
representation in string form.

> Edge case: Empty string coercion is the root of all evil.

This is because every value that needs to be primitive first coerces to string, and the specification dictates
that when converting an empty string to a number we get zero. Number('') === 0;

### toBoolean()

Abstract operation that is basically a lookup. If the value is on the falsy list, is false, otherwise is true

Falsy values

- ''
- 0, -0
- null
- NaN
- false
- undefined
