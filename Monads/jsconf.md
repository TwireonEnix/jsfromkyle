# Summary of talk Mo'problems Mo'Nads, in CityJS 2020

[Link to the talk](https://youtu.be/sP7dtZm_Wx0?t=34470)

- Using the right tools in the right way, for the right job.

## Monads

### Why?

Because of category theory i've learned that it does not matter what values are inside the wrappers, o even
what monadic wrappers they are, what matters is the behavior and relationships that they can make. Types and
values:

Types: behaviors that we can expect. Values: Behaviors that we can use.

The monad (type/interface) vs a monad (value);

The monad has 3 laws

- Left identity,
- Right identity
- Associativity laws.

a monad: is a data structure.

> Example in code

```javascript
const three = Just(3); // Monadic wrapper around the value 3
three._inspect(); // Just(3);
Just.is(three); // true
```

Monads and friends. Applicatives, Foldables, Semi-groups. etc. and they always have predictable behavior.

Why monads? How could I..

1. choose a value or fallback?
2. gracefully handle exceptions?
3. manage side effects?

### Value or fallback

> Note: The monad Maybe represents a two probable values, an actual value, or a 'Nothing'

```javascript
const fortyTwo = Maybe.from(42); // if (v) then Just(v) otherwise Nothing()
fortyTwo._inspect(); // Maybe:Just(42)
Maybe(fortyTwo)._inspect(); // Maybe:Just(Maybe:Just(42))
```

```javascript
const emtpy = Maybe.from(null);
empty._inspect(); // Maybe:Nothing()
Maybe(empty)._inspect(); // Maybe:Just(Maybe:Nothing())
```

Real code example of the use of a Maybe monad

> Imperative

```javascript
const val = fileMimeType.match(/someRegex/);
const [, mediaType] = val ? val : [, 'web']; // DefaultTo
```

> Functional

```javascript
const val = fileMimeType.match(/someRegex/);
const [, mediaType] = Maybe.from(val).fold(() => [, 'web'], identity);
```

### Gracefully Handle Exceptions?

Either monad, this will have either the right or the left value. The right value is the container for the
'happy path' scenario and the left is reserved in case of an exception.

```javascript
Either.Left('Oops')._inspect();
// Either:Left('Oops')

Either.Right(42)._inspect();
// Either:Right(42)

Either.fromFoldable(Maybe(42))._inspect(); // Either:Right(42)
```

> Imperative

```javascript
try {
  const data = JSON.parse(json);
  saveToDB(data);
} catch (err) {
  alert(err);
}
```

> Either monad

```javascript
const safe = fn => (...args) => {
  try {
    return Either.Right(fn(...args));
  } catch (e) {
    return Either.Left(e);
  }
};

const parse = safe(JSON.parse);
parse(json).chain(safe(saveToDB)).fold(alert, identity); // if parse fails the .chain is skipped
```

### Manage side effects.

With the IO monad.

```javascript
IO(() => 42)._inspect(); // IO(anonymous function)
IO(() => 42).run(); // 42, run will trigger the side effects
IO.of(42).run(); // 42
```
